#include <random>
#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>
#include <chrono>

using namespace std;

void calc_count(long N, long& counter)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0, 1);
    for (int n = 0; n < N; ++n) {
        double x = dis(gen);
        double y = dis(gen);
        if ((x*x + y*y) < 1)
            ++counter;
    }
}

int main()
{
    long counter = 0;
    vector<thread> thds;
    int n_of_threads = 1;
    vector<long> res(n_of_threads);
    long N = 10000000;

    auto start = chrono::high_resolution_clock::now();

    for (int i = 0 ; i < n_of_threads; ++i)
    {
        thds.emplace_back(calc_count,
                          N/n_of_threads,
                          ref(res[i]));
    }
    for (thread& th : thds) th.join();

    auto end = chrono::high_resolution_clock::now();

    std::cout
      << (double(accumulate(res.begin(), res.end(), 0L))/N)*4
      << std::endl;

    cout << "Elapsed: " <<
            chrono::duration_cast<chrono::milliseconds>(end-start).count()
            << " ms" << endl;
}
