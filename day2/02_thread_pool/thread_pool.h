#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#include "../01_TSQ/thread_safe_queue.h"
#include <functional>
#include <vector>

typedef std::function<void()> task_t;

class thread_pool
{
    thread_safe_queue<task_t> q;
    std::vector<std::thread> workers;

    void worker()
    {
        for(;;)
        {
            task_t task;
            q.pop(task);
            if (!task)
                return;
            task();
        }
    }
public:
    thread_pool(int n_of_workers)
    {
        for (int i = 0 ; i < n_of_workers ; ++i)
            workers.emplace_back(&thread_pool::worker, this);
    }
    thread_pool(thread_pool&) = delete;
    ~thread_pool()
    {
        for (std::thread& th : workers) q.push(nullptr);
        for (std::thread& th : workers) th.join();
    }
    void submit_task(task_t task)
    {
        q.push(task);
    }
};

#endif // THREAD_POOL_H
