#include <iostream>
#include "../zmq/zhelpers.hpp"
#include "../zmq/zmq.hpp"
#include <string>
#include <thread>
#include <stdio.h>

using namespace std;

int main()
{
    cout << "ZMQ render server" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_ROUTER);
    socket.bind("tcp://192.168.1.3:5555");
    while(true)
    {
        getchar();
        for( int i = 0 ; i < 768 ; ++i)
        {
            //get name and id
            string addres = s_recv(socket);
            s_recv(socket);
            string id = s_recv(socket);
            cout << "addr: " << addres << " name: " <<
                     id << endl;
            s_sendmore(socket, addres);
            s_sendmore(socket, "");
            //sending
            s_send(socket, to_string(i) + " " + to_string(50));
            cout << " " << i << " " << flush;
            this_thread::sleep_for(chrono::milliseconds(50));
        }
    }
    return 0;
}


