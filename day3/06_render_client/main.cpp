#include <iostream>
#include "../zmq/zhelpers.hpp"
#include "../zmq/zmq.hpp"
#include "smallpt_lib.hpp"
#include <string>
#include <thread>
#include <stdlib.h>

using namespace std;

int main ()
{
    // scheme:
    // head: 123 ::

    zmq::context_t context(1);
    zmq::socket_t recieve(context, ZMQ_REQ);
    srand(time(NULL));
    cout << "My id:" << s_set_id(recieve) << endl;
    recieve.connect("tcp://192.168.1.3:5555");

    zmq::socket_t sink(context, ZMQ_PUSH);
    sink.connect("tcp://localhost:5556");

    while(true)
    {
        s_send(recieve, "Leszek");
        std::string line_str;
        line_str = s_recv(recieve);
        std::istringstream iss(line_str);
        int line_no;
        int quality;
        iss >> line_no >> quality;
        std::cout << "line number: " << line_no << std::endl;
        std::string output;
        std::string line = scan_line(line_no, quality);
        std::ostringstream oss;
        oss << "L3szek: " << line_no << " :: " << line;
        //std::cout << "output: " << oss.str() << std::endl;
        s_send(sink, oss.str());
    }
}


