#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>

using namespace std;
using namespace AmqpClient;

string ex_name = "Nokia-test";
string q_name = "Nokia-q";

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create();

    // setup
    channel->DeclareExchange(ex_name);
    channel->DeclareQueue(q_name, false, true, false, false);
    channel->BindQueue(q_name, ex_name, "chat");

    // send message

    channel->BasicPublish(ex_name, "chat",
                          BasicMessage::Create("medium is the message"));
}

void receive()
{
    Channel::ptr_t channel;
    channel = Channel::Create();
    channel->BasicConsume(q_name);
    while(true)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    send();
    receive();
    return 0;
}

