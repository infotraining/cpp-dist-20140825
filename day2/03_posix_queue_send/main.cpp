#include <iostream>
#include <mqueue.h>
#include <string>
#include <string.h>

using namespace std;

int main()
{
    cout << "Hello posix queue" << endl;
    mqd_t id = mq_open("/myqueue1", O_RDWR | O_CREAT,
                       0666, NULL);
    if (id == -1)
    {
        cerr << "Error opening queue" << endl;
    }
    string msg = "Hello World\n";
    mq_send(id, msg.c_str(), msg.size()+1, 1);
    mq_close(id);
    return 0;
}

