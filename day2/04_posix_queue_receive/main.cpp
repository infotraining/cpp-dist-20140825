#include <iostream>
#include <mqueue.h>
#include <string>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main()
{
    cout << "Hello posix queue - reader" << endl;
    mq_unlink("/myqueue1");

    struct mq_attr attrs;
    memset(&attrs, 0, sizeof(attrs));
    attrs.mq_maxmsg = 10;
    attrs.mq_msgsize = 2048;

    mqd_t id = mq_open("/myqueue1", O_RDWR | O_CREAT,
                       0666, &attrs);
    if (id == -1)
    {
        perror("error creating");
        return 0;
    }

    mq_getattr(id, &attrs);
    cout << "params: "
         << "message size: " << attrs.mq_msgsize
         << "  message queue lenght: " << attrs.mq_maxmsg << endl;

    char  buff[8192];
//    int size = mq_receive(id, buff, sizeof(buff), NULL);
//    if (size == -1)
//    {
//        cerr << "Error in read - got -1" << endl;
//    }
//    cout << "Got " << buff << endl;
    mq_close(id);
    return 0;
}

