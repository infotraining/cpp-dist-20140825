#include <iostream>
#include "mq_long.h"
#include <mqueue.h>
#include <vector>
#include <thread>

using namespace std;

mq_long *inq;
mq_long *outq;

void producer()
{
    for (int i = 10000000; i < 10010000 ; ++i)
        inq->send(i);
}

void sink()
{
    for (;;)
    {
        cout << outq->get() << " : " << flush;
    }
}

int main()
{
    mq_unlink("/input_queue");
    mq_unlink("/output_queue");

    inq = new mq_long("/input_queue");
    outq = new mq_long("/output_queue");
    cout << "Producer and sink" << endl;
    thread prodth(producer);
    thread sinkth(sink);
    prodth.join();
    sinkth.join();
    delete inq;
    delete outq;
    return 0;
}

