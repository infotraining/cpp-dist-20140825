#include <iostream>
#include <functional>
#include "thread_pool.h"

using namespace std;

//class ThreadPool
//{
//public:
//    void submit_task(Runnable * task)
//    {
//        task->run();
//        delete task;
//    }
//};

//class Runnable
//{
//    virtual run();
//};

//... void submit_task(std::function<void()> task);

int main()
{
    thread_pool tp(2);
    for (int i = 0 ; i < 10 ; ++i)
        tp.submit_task([i] () {
            cout << "Hello from lambda " << i << endl;
            this_thread::sleep_for(chrono::milliseconds(500));
        });
    return 0;
}

