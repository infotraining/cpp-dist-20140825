#include <iostream>
#include "mq_long.h"

using namespace std;

bool is_prime(unsigned long v)
{
    for(int d=2; d<v; ++d)
        if( v % d == 0)
            return false;

    return true;
}

int main()
{
    mq_long inq("/input_queue");
    mq_long outq("/output_queue");
    for(;;)
    {
        long num = inq.get();
        if (is_prime(num))
            outq.send(num);
    }

    return 0;
}

