#include <iostream>
#include <string>
#include <thread>
#include <mutex>

using namespace std;

class BankAccount
{
    mutex mtx_;
    double balance_;
    string owner_;
public:
    BankAccount(const string& owner, double balance) :
        owner_(owner), balance_(balance)
    {
    }

    void print()
    {
        cout << "owner: " << owner_ <<
                "  balance " << balance_ << endl;
    }

    void transfer(BankAccount& other, double amount)
    {
        unique_lock<mutex> ll(mtx_, defer_lock);
        unique_lock<mutex> ol(other.mtx_, defer_lock);
        lock(ll, ol);
        other.balance_ -= amount;
        balance_ += amount;
    }
};

void test_transfer(BankAccount& ba1, BankAccount& ba2)
{
    for (int i = 0 ; i < 10000 ; ++i)
        ba1.transfer(ba2, 1);
}

int main()
{
    cout << "Hello World!" << endl;
    BankAccount ba1("Leszek", 10000);
    BankAccount ba2("other", 10000);
    thread th1(test_transfer, ref(ba1), ref(ba2));
    thread th2(test_transfer, ref(ba2), ref(ba1));
    th1.join();
    th2.join();
    ba1.print();
    ba2.print();
    return 0;
}

