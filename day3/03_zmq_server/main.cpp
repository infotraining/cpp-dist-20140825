#include <iostream>
#include "../zmq/zhelpers.hpp"
#include "../zmq/zmq.hpp"

using namespace std;

int main()
{
    cout << "ZMQ server" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REP);
    socket.bind("tcp://*:5555");
    while(true)
    {
        cout << "From client " << s_recv(socket) << endl;
        s_send(socket, "msg from server");
    }
    return 0;
}

