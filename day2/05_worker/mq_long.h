#ifndef MQ_LONG_H
#define MQ_LONG_H

#include <mqueue.h>
#include <string>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

class mq_long
{
    mqd_t id;
public:
    mq_long(const std::string& name)
    {
        struct mq_attr attrs;
        memset(&attrs, 0, sizeof(attrs));
        attrs.mq_maxmsg = 10;
        attrs.mq_msgsize = sizeof(long);

        id = mq_open(name.c_str(), O_RDWR | O_CREAT,
                           0666, &attrs);
        if (id == -1)
        {
            perror("error creating");
            throw std::exception();
        }
    }

    void send(long n)
    {
        mq_send(id, (char*)(&n), sizeof(long), 1);
    }

    long get()
    {
        long res;
        mq_receive(id, (char*)(&res), sizeof(long), NULL);
        return res;
    }

    ~mq_long()
    {
        mq_close(id);
    }
};

#endif // MQ_LONG_H
