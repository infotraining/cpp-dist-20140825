#include <iostream>
#include <thread>
#include "thread_safe_queue.h"

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for (int i = 0 ; i < 100 ; ++i)
        q.push(i);
}

void consumer(int id)
{
    for(;;)
    {
        int msg;
        q.pop(msg);
        cout << id << " got message " << msg << endl;
    }
}

int main()
{
    cout << "Prod-cons" << endl;
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    thds.emplace_back(consumer, 3);
    for (thread& th : thds) th.join();
    return 0;
}

