#include <iostream>
#include <vector>
#include <thread>

using namespace std;

void hello(int id)
{
    this_thread::sleep_for(chrono::seconds(1));
    cout << "Hello from thread " << id << endl;
}

class Functor
{
public:
    void operator()()
    {
        this_thread::sleep_for(chrono::seconds(1));
        cout << "Hello from functor" << endl;
    }
};

thread generate_thread()
{
    int a = 10;
    thread th(&hello, a);
    thread th2;
    th2 = move(th);
    return th2;
}

int main()
{
    Functor f{};
    cout << "Hello World!" << endl;

    vector<thread> threads;


    threads.emplace_back(&hello, 1);
    threads.emplace_back(f);
    threads.emplace_back( [] () { cout << "from lambda" << endl;});
    threads.push_back(generate_thread());
    for (thread& th : threads) th.join();
    return 0;
}

