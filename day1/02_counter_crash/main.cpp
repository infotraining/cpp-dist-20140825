#include <iostream>
#include <thread>
#include <atomic>
#include <mutex>

using namespace std;

//atomic<long> counter(0);
long counter = 0;
mutex mtx;

void increase()
{
    for (int i = 0 ; i < 1000000 ; ++i)
    {
        lock_guard<mutex> l(mtx);
        ++counter;
//        if (counter == 1000)
//            return;
    }
}

int main()
{
    cout << "Hello World!" << endl;

    auto start = chrono::high_resolution_clock::now();

    thread th1(increase);
    thread th2(increase);
    th1.join();
    th2.join();

    auto end = chrono::high_resolution_clock::now();


    cout << "Counter = " << counter << endl;

    cout << "Elapsed: " <<
            chrono::duration_cast<chrono::milliseconds>(end-start).count()
            << " ms" << endl;
    return 0;
}

