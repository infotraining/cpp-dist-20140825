## Multithreading and distributed programming

### Additonal information

* login and password for VM:

```
dev  /  tymczasowe
```

* reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

* proxy settings

We can add, .profile

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

* proxy for APT

```
/etc/apt/apt.conf

Acquire::http::proxy "http://10.144.1.10:8080/";
Acquire::https::proxy "https://10.144.1.10:8080/";
Acquire::ftp::proxy "ftp://10.144.1.10:8080/";
```

* git

```
git-cheat-sheet
http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf

git status  # local repository status
git pull    # getting files from repository
git stash   # moving modifications to stash
```