#include <iostream>
#include <queue>
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex mtx;
condition_variable cond;

void producer()
{
    for (int i = 0 ; i < 1000 ; ++i)
    {
        {
            lock_guard<mutex> l(mtx);
            q.push(i);
            cond.notify_one();
        }
        this_thread::sleep_for(chrono::milliseconds(100));
    }
}

void consumer(int id)
{
    while(true)
    {
        unique_lock<mutex> l(mtx);
        cond.wait(l, [] () { return !q.empty();});
        int msg = q.front();
        q.pop();
        cout << "Consumer " << id <<
                " got the message " << msg << endl;
    }
}

int main()
{
    cout << "Prod-cons" << endl;
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    thds.emplace_back(consumer, 3);
    for (thread& th : thds) th.join();
    return 0;
}

