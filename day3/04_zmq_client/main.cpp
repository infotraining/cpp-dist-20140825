#include <iostream>
#include "../zmq/zhelpers.hpp"
#include "../zmq/zmq.hpp"

using namespace std;

int main()
{
    cout << "ZMQ client" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ);
    socket.connect("tcp://192.168.1.3:5555");
    for (int i = 0 ; i < 10 ; ++i)
    {
        s_send(socket, "Hello my dear server");
        cout << "From server " << s_recv(socket) << endl;
    }
    return 0;
}
