#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>

using namespace std;
using namespace AmqpClient;

string ex_name = "Nokia-test";
string q_name = "Nokia-q";

int main()
{
    Channel::ptr_t channel;
    channel = Channel::Create("192.168.1.3", 5672,
                              "admin", "tymczasowe");
    std::string uid = "[ Leszek ] : ";
    for(;;)
    {

        std::string msg;
        cout << ">>> ";
        std::getline(std::cin, msg);
        msg = uid + msg;
        channel->BasicPublish(ex_name, "chat", BasicMessage::Create(msg));

    }
    return 0;
}

